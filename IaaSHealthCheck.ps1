function checkhosts {
    $vbclusters = Get-Cluster
    foreach($c in $vbclusters) {
        $clusterhosts = $c|get-vmhost
        Write-host "===========" $c "=============="
        foreach($h in $clusterhosts) {
            $memUse = $h.MemoryUsageGB
            $memTot = $h.MemoryTotalGB
            $cpuUse = $h.CpuUsageMhz
            $cpuTot = $h.CpuTotalMhz 
            if($memTot -eq 0) {
                Write-Host $h ":" "Host is turned-off" -ForegroundColor Red
            }
            else {
                $cpuPerc = [math]::Round(($cpuUse/$cpuTot)*100)
                $memPerc = [math]::Round(($memUse/$memTot)*100)
                Write-Host $h ":" "cpu-"$cpuPerc"%" "mem-"$memPerc -ForegroundColor Green
            }
        }
    }
} 