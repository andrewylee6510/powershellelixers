# Function checkhosts assumes that you are already connected to vCenter(s).

function checkhosts {
    $naughtylist = [System.Collections.ArrayList]@()
    $vbclusters = Get-Cluster
    foreach($c in $vbclusters) {
        $clusterhosts = $c|Get-VMHost
        Write-Host "====================" $c "===================="
        foreach($h in $clusterhosts) {
            $hostname = $h.name
            $memUse = $h.MemoryUsageGB
            $memTot = $h.MemoryTotalGB
            $cpuUse = $h.CpuUsageMhz
            $cpuTot = $h.CpuTotalMhz
            if(($memTot -eq 0) -or ($cpuUse -eq 0)) {
                Write-Host $h ":" "Host error. Contact Administrator for details." -ForegroundColor Red
                $naughtylist.add($hostname)
            }
            else {
                $cpuPerc = [math]::Round(($cpuUse/$cpuTot)*100)
                $memPerc = [math]::Round(($memUse/$memTot)*100)
                if(($cpuPerc -eq 0) -and ($memPerc -eq 0)) {
                    Write-Host $h ":" "cpu-"$cpuPerc"%" "mem-"$memPerc"%" -ForegroundColor Red
                    $naughtylist.add($hostname)
                }
                else {
                    Write-Host $h ":" "cpu-"$cpuPerc"%" "mem-"$memPerc"%" -ForegroundColor Green
                }            
            }
        }
    }
    Write-Host "====================" "Naughty List" "===================="
    foreach($badhost in $naughtylist) {
        $badhost
    }
}

# vchealth checks for vchaHealth checks to see that vcha is running okay
function vchahealth {
    
}

#guestintrospection
