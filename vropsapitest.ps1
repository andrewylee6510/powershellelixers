<#
$body = @{
    "username"=
    "password"=
} | ConvertTo-Json

$header = @{
    "Accept"="application/json"
    "Content-Type"="application/json"
}
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

Invoke-WebRequest -Uri https:///suite-api/doc/rest/index.html -Method Get
#>

$urlsend = 'https:///suite-api/api/alerts'
$creds = Get-Credential

$ContentType = "application/xml;charset=utf-8"
$header = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$header.Add("Accept", 'application/xml')
$header.Add("X-vRealizeOps-API-use-unsupported", 'true')


[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$result = Invoke-RestMethod -Method GET -Uri $urlsend -Credential $creds -ContentType $ContentType -Headers $header

Write-Output $result.InnerXml

<#
try {
    $result = Invoke-RestMethod -Method GET -Uri $urlsend -Credential $creds -ContentType $ContentType -Headers $header
}
catch {
    $result = $_.Exception.Response.GetResponseStream()
    $reader = New-Object System.IO.StreamReader($result)
    $reader.BaseStream.Position = 0
    $reader.DiscardBufferedData()
    $responseBody = $reader.ReadToEnd();
}
#>